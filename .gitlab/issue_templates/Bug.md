<!---
Please read this!

The [minimum required GitLab version](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/README.md#minimum-supported-version) is GitLab 13.4. If you're experiencing an issue and running a self-managed version older than 13.4, please test against GitLab.com prior to submitting your bug report.

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label:

- https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues?label_name%5B%5D=bug

and verify the issue you're about to submit isn't a duplicate.

If you are facing issues around configuring Token from your GitLab.com account, see the list of already addressed Token related issues:

https://gitlab.com/gitlab-org/gitlab-vscode-extension/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=token-issue
--->

### Summary

<!-- Summarize the bug encountered concisely -->

### Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

### What is the current *bug* behavior?

<!-- What actually happens -->

### What is the expected *correct* behavior?

<!-- What you should see instead -->

### Relevant logs and/or screenshots

<!-- Logs can be found by running `GitLab: Show extension logs` command (using `cmd+shift+p`) -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

/label ~bug ~"devops::create"  ~"group::code review"  ~"Category:Editor Extension" ~"VS Code"
